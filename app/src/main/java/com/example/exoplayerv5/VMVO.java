package com.example.exoplayerv5;

public class VMVO {
    int no;
    String aminationUri;
    String musicUri;
    String id;

    public VMVO() {
    }

    public VMVO(int no, String aminationUri, String musicUri, String id) {
        this.no = no;
        this.aminationUri = aminationUri;
        this.musicUri = musicUri;
        this.id = id;
    }

    public int getNo() {
        return no;
    }

    public String getAminationUri() {
        return aminationUri;
    }

    public String getMusicUri() {
        return musicUri;
    }

    public String getId() {
        return id;
    }

    public void setNo(int no) {
        this.no = no;
    }

    public void setAminationUri(String aminationUri) {
        this.aminationUri = aminationUri;
    }

    public void setMusicUri(String musicUri) {
        this.musicUri = musicUri;
    }

    public void setId(String id) {
        this.id = id;
    }
}
