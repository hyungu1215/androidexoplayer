package com.example.exoplayerv5;

import androidx.appcompat.app.AppCompatActivity;

import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.google.android.exoplayer2.ExoPlaybackException;
import com.google.android.exoplayer2.MediaItem;
import com.google.android.exoplayer2.PlaybackParameters;
import com.google.android.exoplayer2.Player;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.Timeline;
import com.google.android.exoplayer2.source.TrackGroupArray;
import com.google.android.exoplayer2.trackselection.TrackSelectionArray;
import com.google.android.exoplayer2.ui.PlayerView;

import java.io.File;
import java.util.ArrayList;
import java.util.List;


public class MainActivity extends AppCompatActivity {

    private PlayerView animationPlayerView;
    private SimpleExoPlayer animationPlayer;

    private PlayerView musicPlayerView;
    private SimpleExoPlayer musicPlayer;

    ProgressBar progressBar;

    ImageView btStop;
    ImageView btPrev;
    ImageView btNext;
    ImageView logo;
    ImageView va_play;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        animationPlayerView = findViewById(R.id.animation_view);
        musicPlayerView = findViewById(R.id.music_view);

        progressBar = findViewById(R.id.progress_bar);

        btStop = findViewById(R.id.bt_stop);
        btPrev = findViewById(R.id.bt_prev);
        btNext = findViewById(R.id.bt_next);
        logo = findViewById(R.id.logo);
        va_play = findViewById(R.id.va_play);

        initializePlayer();
    }

    private void initializePlayer() {
        btNext.setImageDrawable(getResources().getDrawable(R.drawable.ic_next));
        logo.setVisibility(View.GONE);
        va_play.setVisibility(View.GONE);

        animationPlayer = new SimpleExoPlayer.Builder(this).build();
        animationPlayerView.setPlayer(animationPlayer);

        musicPlayer = new SimpleExoPlayer.Builder(this).build();
        musicPlayerView.setPlayer(musicPlayer);

        animationPlayer.addListener(new Player.EventListener() {
            @Override
            public void onTimelineChanged(Timeline timeline, Object manifest, int reason) {

            }

            @Override
            public void onTracksChanged(TrackGroupArray trackGroups, TrackSelectionArray trackSelections) {

            }

            @Override
            public void onLoadingChanged(boolean isLoading) {

            }

            @Override
            public void onPlayerStateChanged(boolean playWhenReady, int playbackState) {
                // Check condition
                if (playbackState == Player.STATE_BUFFERING) {
                    // WHen buffering
                    // Show progress bar
                    progressBar.setVisibility(View.VISIBLE);
                } else if (playbackState == Player.STATE_READY) {
                    // When ready
                    // Hide progress bar
                    progressBar.setVisibility(View.GONE);
                    if (!animationPlayer.hasPrevious()) {
                        btPrev.setImageDrawable(getResources().getDrawable(R.drawable.ic_no_previous));
                    } else {
                        btPrev.setImageDrawable(getResources().getDrawable(R.drawable.ic_previous));
                    }
                    if (!animationPlayer.hasNext()) {
                        btNext.setImageDrawable(getResources().getDrawable(R.drawable.ic_no_next));
                    } else {
                        btNext.setImageDrawable(getResources().getDrawable(R.drawable.ic_next));
                    }
                }
            }

            @Override
            public void onRepeatModeChanged(int repeatMode) {

            }

            @Override
            public void onShuffleModeEnabledChanged(boolean shuffleModeEnabled) {

            }

            @Override
            public void onPlayerError(ExoPlaybackException error) {

            }

            @Override
            public void onPositionDiscontinuity(int reason) {

            }

            @Override
            public void onPlaybackParametersChanged(PlaybackParameters playbackParameters) {

            }

            @Override
            public void onSeekProcessed() {

            }
        });

        File file = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS), "/dizzy.mp4");
        String url = file.getPath(); // /storage/emulated/0/Download/dizzy.mp4
        List<VMVO> vmVoList = new ArrayList<VMVO>();
        VMVO vmvo0 = new VMVO();
        vmvo0.setAminationUri("/storage/emulated/0/Download/" + "dizzy.mp4");
        vmvo0.setMusicUri("/storage/emulated/0/Download/" + "dizzy.mp4");
        vmVoList.add(vmvo0);
        /*
        VMVO vmvo1 = new VMVO();
        vmvo1.setAminationUri("android.resource://" + getPackageName() + "/" + R.raw.video);
        vmvo1.setMusicUri("android.resource://" + getPackageName() + "/" + R.raw.audio);
        VMVO vmvo2 = new VMVO();
        vmvo2.setAminationUri("android.resource://" + getPackageName() + "/" + R.raw.audio);
        vmvo2.setMusicUri("android.resource://" + getPackageName() + "/" + R.raw.video);
        VMVO vmvo3 = new VMVO();
        vmvo3.setAminationUri("https://html5demos.com/assets/dizzy.mp4");
        vmvo3.setMusicUri("android.resource://" + getPackageName() + "/" + R.raw.audio);
        vmVoList.add(vmvo0);
        vmVoList.add(vmvo1);
        vmVoList.add(vmvo2);
        vmVoList.add(vmvo3);
        */
        for (int i = 0; i < vmVoList.size(); i++) {
            Uri animationUri = Uri.parse(vmVoList.get(i).getAminationUri());
            Uri musicUri = Uri.parse(vmVoList.get(i).getMusicUri());

            MediaItem animationMediaItem = MediaItem.fromUri(animationUri);
            MediaItem musicMediaItem = MediaItem.fromUri(musicUri);

            animationPlayer.addMediaItem(animationMediaItem);
            musicPlayer.addMediaItem(musicMediaItem);

            musicPlayer.prepare();
            animationPlayer.prepare();

            musicPlayer.setPlayWhenReady(true);
            animationPlayer.setPlayWhenReady(true);
        }

        btStop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onStop();
            }
        });

        btPrev.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                animationPlayer.previous();
                musicPlayer.previous();
            }
        });

        btNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                animationPlayer.next();
                musicPlayer.next();
            }
        });
    }

    @Override
    public void onStop() {
        super.onStop();
        releasePlayer();
    }

    private void releasePlayer() {
        if (animationPlayer != null) {
            animationPlayer.release();
            musicPlayer.release();

            animationPlayer = null;
            musicPlayer = null;

            logo.setVisibility(View.VISIBLE);
            va_play.setVisibility(View.VISIBLE);

            btPrev.setOnClickListener(null);
            btNext.setOnClickListener(null);
            va_play.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    initializePlayer();
                }
            });
        }
    }
}
